import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from "../../shared/services/auth-service.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() openSummaryDialogEmit = new EventEmitter<void>();

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  openSummaryDialog() {
    this.openSummaryDialogEmit.emit();
  }

  logout() {
    this.authService.logout();
  }
}