import { Component, Inject, OnInit } from '@angular/core';
import { IList } from "../../shared/interfaces/lists.interface";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-summary-dialog',
  templateUrl: './summary-dialog.component.html',
  styleUrls: ['./summary-dialog.component.scss']
})
export class SummaryDialogComponent implements OnInit {
  public lists: IList[]

  constructor(@Inject(MAT_DIALOG_DATA) public data: {lists: IList[] }) {
  }

  ngOnInit(): void {
    this.lists = JSON.parse(JSON.stringify(this.data.lists))
    this.sortLists();
  }

  sortLists() {
    this.lists.forEach(list => {
      list.tasks.sort((a, b) => {
        if (a.finishDate && b.finishDate) {
          return b.finishDate.valueOf() - new Date(a.finishDate).valueOf();
        } else if(a.finishDate && !b.finishDate){
          return a.finishDate ? -1 : b.finishDate ? 1 : 0
        } else if (!a.finishDate && b.finishDate) {
          return a.finishDate ? -1 : b.finishDate ? 1 : 0
        } else{
          return b.initDate.valueOf() - new Date(a.initDate).valueOf();
        }
      })
    })
  }
}
