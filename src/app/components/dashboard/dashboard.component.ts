import { Component, OnInit } from '@angular/core';
import { IList } from "../../shared/interfaces/lists.interface";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { FormControl, Validators } from "@angular/forms";
import { ListsService } from "../../shared/services/lists.service";
import { take } from "rxjs/operators";
import { DialogService } from "../../shared/services/dialog.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public lists: IList[];
  public newTaskListIndex: number | null = null;
  public newListControl = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(private listsService: ListsService, private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.listsService.getLists().pipe(take(1)).subscribe(listsData => {
      this.lists = listsData
    })
  }

  showNewTaskForm(index: number): void {
    this.newTaskListIndex = index;
  }

  isNewTask(index: number): boolean {
    return this.newTaskListIndex === index;
  }

  addNewTask(taskData: {title: string; description: string}) {
    if (taskData && this.newTaskListIndex !== null) {
      this.lists[this.newTaskListIndex].tasks.push({
        id: this.lists[this.newTaskListIndex].tasks.length,
        isDone: false,
        initDate: new Date(),
        ...taskData
      });
      this.newTaskListIndex = null;
    } else {
      this.newTaskListIndex = null;
    }
  }

  deleteTask(listIndex: number, taskIndex: number): void {
    this.lists[listIndex].tasks.splice(taskIndex, 1)
  }

  deleteList(listIndex: number): void {
    this.lists.splice(listIndex, 1);
  }

  addList() {
    this.lists.push({
      id: this.lists.length,
      name: this.newListControl.value,
      filter: 'all',
      tasks: []
    });
    this.newListControl.reset();
  }

  updateIsDoneTask(newValue: boolean, listIndex: number, taskIndex: number): void {
    this.lists[listIndex].tasks[taskIndex].isDone = newValue;

    if (newValue) {
      this.lists[listIndex].tasks[taskIndex].finishDate = new Date();
    } else {
      this.lists[listIndex].tasks[taskIndex].finishDate = null;
    }
    this.resetFilteredLists(listIndex);
  }

  updateFilteredLists(index: number, isDone: boolean) {
    this.lists[index].filter = isDone;
  }

  resetFilteredLists(index: number) {
    this.lists[index].filter = 'all';
  }

  drop(event: CdkDragDrop<any>, listIndex: number) {
    moveItemInArray(this.lists[listIndex].tasks, event.previousIndex, event.currentIndex);
    this.resetFilteredLists(listIndex);
  }

  openSummaryDialog() {
    this.dialogService.openSummaryDialog(this.lists);
  }
}
