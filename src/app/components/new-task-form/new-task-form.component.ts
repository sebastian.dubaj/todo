import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-new-task-form',
  templateUrl: './new-task-form.component.html',
  styleUrls: ['./new-task-form.component.scss']
})
export class NewTaskFormComponent{
  @Output() newTaskEvent = new EventEmitter<any>();
  titleControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  descriptionControl = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor() { }

  addTask() {
    this.newTaskEvent.emit({
      title: this.titleControl.value,
      description: this.descriptionControl.value
    })
  }

  closeNewTask() {
    this.newTaskEvent.emit(null);
  }
}
