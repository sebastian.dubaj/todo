import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../shared/services/auth-service.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  emailControl = new FormControl('', [Validators.required, Validators.email]);
  passwordControl = new FormControl('', [Validators.required, Validators.minLength(6)]);
  showLoginError = false;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.logout();
  }

  login() {
    this.authService.login(this.emailControl.value, this.passwordControl.value)
      .catch(() => this.showLoginError = true )
  }
}
