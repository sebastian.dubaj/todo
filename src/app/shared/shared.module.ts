import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from "@angular/material/toolbar";
import { NewListDialogComponent } from './components/new-list-dialog/new-list-dialog.component';
import { FilterByPipe } from './pipes/filter-by.pipe';



@NgModule({
  declarations: [
    NewListDialogComponent,
    FilterByPipe
  ],
  exports: [
    FilterByPipe
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
  ]
})
export class SharedModule { }
