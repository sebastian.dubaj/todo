import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { take } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authState$: Observable<any | null> = this.fireAuth.authState;

  constructor(private fireAuth: AngularFireAuth, private router: Router) {
  }

  login(email: string, password: string) {
    return this.fireAuth.signInWithEmailAndPassword(email, password)
      .then(data => {
        localStorage.setItem('user', JSON.stringify(data.user))
        this.router.navigate(['/dashboard']);
      })
  }

  logout() {
    this.fireAuth.signOut().then(() => {
          localStorage.removeItem('user');
          this.router.navigate(['/login']);
        }
      )
      .catch(error => console.error(error))
  }
}
