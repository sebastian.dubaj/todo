import { Injectable } from '@angular/core';
import { listsMock } from "./lists.mock";
import { Observable, of } from "rxjs";
import { IList } from "../interfaces/lists.interface";

@Injectable({
  providedIn: 'root'
})
export class ListsService {

  constructor() { }

  getLists(): Observable<IList[]> {
    return of(listsMock);
  }
}
