import { Injectable } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { SummaryDialogComponent } from "../../components/summary-dialog/summary-dialog.component";
import { IList } from "../interfaces/lists.interface";

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialog: MatDialog) {}

  openSummaryDialog(lists: IList[]): void {
    this.dialog.open(SummaryDialogComponent, {
      width: '900px',
      data: {
        lists: lists
      }
    });
  }
}
