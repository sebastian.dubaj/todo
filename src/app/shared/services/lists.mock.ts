import { IList } from "../interfaces/lists.interface";

export const listsMock: IList[] = [
  {
    id: 1,
    name: 'Dom',
    filter: 'all',
    tasks: [
      {
        id: 1,
        title: 'Obiad',
        description: 'Ugotować obiad',
        isDone: true,
        initDate: new Date(new Date().setHours(new Date().getHours() - 1)),
        finishDate: new Date(),
      },
      {
        id: 2,
        title: 'Pranie',
        description: 'Zrobić pranie',
        initDate: new Date(new Date().setHours(new Date().getHours() - 4)),
        isDone: false,
      },
      {
        id: 3,
        title: 'Sprzątanie',
        description: 'Posprzątać w mieszkaniu',
        initDate: new Date(new Date().setHours(new Date().getHours() - 3)),
        isDone: true,
        finishDate: new Date(new Date().setHours(new Date().getHours() - 2)),
      },
      {
        id: 4,
        title: 'Naprawa drzwi',
        description: 'Zamek w łazience nie działa',
        initDate: new Date(new Date().setHours(new Date().getHours() - 3)),
        isDone: true,
        finishDate: new Date(new Date().setHours(new Date().getHours() - 3)),
      },
    ]
  },
  {
    id: 2,
    name: 'Zakupy',
    filter: 'all',
    tasks: [
      {
        id: 1,
        title: 'Woda',
        description: '6x Cisowianka niegazowana',
        initDate: new Date(new Date().setHours(new Date().getHours() - 1)),
        isDone: true,
        finishDate: new Date()
      },
      {
        id: 2,
        title: 'Sok',
        description: '2x sok jabłkowy 100%',
        initDate: new Date(new Date().setHours(new Date().getHours() - 2)),
        isDone: false,
      },
      {
        id: 3,
        title: 'Chleb',
        description: '1x Chleb pełnoziarnisty',
        initDate: new Date(new Date().setHours(new Date().getHours() - 3)),
        isDone: false,
      },
    ]
  },
]