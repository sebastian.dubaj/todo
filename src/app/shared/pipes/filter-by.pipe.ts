import { Pipe, PipeTransform } from '@angular/core';
import { ITask } from "../interfaces/lists.interface";

@Pipe({
  name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

  transform(items: ITask[], filter: any): ITask[] {
    if (!items || !filter) {
      return items;
    }
    if(filter === true){
      return items.filter(item => item.isDone);
    } else if (filter === false){
      return items.filter(item => !item.isDone);
    } else {
      return items;
    }
  }
}
