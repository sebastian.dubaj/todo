export interface IList {
  id: number;
  name: string;
  filter: 'all' | boolean;
  tasks: ITask[];
}

export interface ITask {
  id:number;
  title: string;
  description: string;
  isDone: boolean;
  initDate: Date;
  finishDate?: Date | null;
}
